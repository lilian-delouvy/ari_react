﻿import {Link} from "react-router-dom";
import './Home.scss';

const Home = () => {
    return(
        <div className="home">
            <Link to="/basics">
                <button className="home__button">Bases</button>
            </Link>
            <Link to="/connectFour">
                <button className="home__button">Puissance 4</button>
            </Link>
            <Link to="/other">
                <button className="home__button">Horloge</button>
            </Link>
        </div>
    );
};

export default Home;