import logo from './logo.svg';
import './App.css';
import {BrowserRouter} from "react-router-dom";
import Routes from "./Routes";

function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo"/>
      <BrowserRouter>
          <Routes/>
      </BrowserRouter>
    </div>
  );
}

export default App;
