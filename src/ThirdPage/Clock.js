﻿import Title from "../Title";
import {useEffect, useState} from "react";
import './Clock.scss';

const Clock = () => {
    
    const [state, setState] = useState({
        date: new Date().toLocaleTimeString()
    });
    
    useEffect(() => {
        const timerID = setInterval(() => tick(), 1000);
        return function cleanup(){
            clearInterval(timerID);
        };
    });
    
    const tick = () => {
        setState({...state, date: new Date().toLocaleTimeString()});
    }
    
    return(
        <>
            <Title text="Horloge"/>
            <a className="App-link" href="/">Retour à la page principale</a>
            <div className="clock__text">Il est {state.date} !</div>
        </>
    );
};

export default Clock;