﻿import Square from "./Square";
import './ConnectFour.scss';
import {useState} from "react";
import Title from "../Title";

const ConnectFour = () => {

    const [state, setState] = useState(
        {
            squares: Array(42).fill(0), //there are 42 squares in a connect four game
            greenIsNext: true,
            greenVictories: 0,
            redVictories: 0,
            hasBeenUpdated: false
        }
    );

    const isOnRow = (value) => {
        const correctValues = [0, 1, 2, 3, 7, 8, 9, 10, 14, 15, 16, 17, 21, 22, 23, 24, 28, 29, 30, 31, 35, 36, 37, 38];
        return correctValues.includes(value);
    };
    
    const isOnColumn = (value) => {
        const correctValues = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
        return correctValues.includes(value);
    };
    
    const isOnLeftDiag = (value) => {
        const correctValues = [0,1,2,3,7,8,9,10,14,15,16,17];
        return correctValues.includes(value);
    };
    
    const isOnRightDiag = (value) => {
        const correctValues = [3,4,5,6,10,11,12,13,17,18,19,20];
        return correctValues.includes(value);
    };

    const getWinner = () => {
        const tempSquares = state.squares;
        for (let i = 0; i < 42; i++) {
            if (isOnRow(i)) {
                for (let j = 1; j < 3; j++) {
                    if (tempSquares[i] === j && tempSquares[i + 1] === j && tempSquares[i + 2] === j && tempSquares[i + 3] === j) {
                        return j === 1 ? "vert" : "rouge";
                    }
                }
            }
            if(isOnColumn(i)){
                for(let j = 1; j < 3; j++){
                    if(tempSquares[i] === j && tempSquares[i+7] === j && tempSquares[i + 14] === j && tempSquares[i + 21] === j){
                        return j === 1 ? "vert" : "rouge";
                    }
                }
            }
            if(isOnLeftDiag(i)){
                for(let j = 1; j < 3; j++){
                    if(tempSquares[i] === j && tempSquares[i+8] === j && tempSquares[i + 16] === j && tempSquares[i + 24] === j){
                        return j === 1 ? "vert" : "rouge";
                    }
                }
            }
            if(isOnRightDiag(i)){
                for(let j = 1; j < 3; j++){
                    if(tempSquares[i] === j && tempSquares[i+6] === j && tempSquares[i + 12] === j && tempSquares[i + 18] === j){
                        return j === 1 ? "vert" : "rouge";
                    }
                }
            }
        }
        return null;
    };
    
    const updateVictories = () => {
        const winner = getWinner();
        if(winner != null && !state.hasBeenUpdated){
            winner === "vert" ?
                setState({...state, greenVictories: state.greenVictories + 1, hasBeenUpdated: true}) :
                setState({...state, redVictories: state.redVictories + 1, hasBeenUpdated: true});
        }
        return winner;
    }

    const handleClick = (columnValue) => {
        const newSquares = state.squares;
        if (getWinner(newSquares) != null) {
            return;
        }
        const lastSquareOfColumn = columnValue === 0 ? 35 : columnValue + 7 * 5;
        for (let i = lastSquareOfColumn; i >= 0; i -= 7) {
            if (newSquares[i] === 0) {
                newSquares[i] = state.greenIsNext ? 1 : 2;
                break;
            }
        }
        setState({...state, squares: newSquares, greenIsNext: !state.greenIsNext});
    };

    const createNewGame = () => {
        setState({...state, squares: Array(42).fill(0), hasBeenUpdated: false});
    };
    
    const winner = updateVictories();

    return (
        <>
            <Title text="Puissance 4"/>
            <div className="connect-four__row">
                <a className="App-link" href="/">Retour à la page principale</a>
            </div>
            <div className="connect-four__row">
                <Square value={state.squares[0]}/>
                <Square value={state.squares[1]}/>
                <Square value={state.squares[2]}/>
                <Square value={state.squares[3]}/>
                <Square value={state.squares[4]}/>
                <Square value={state.squares[5]}/>
                <Square value={state.squares[6]}/>
            </div>
            <div className="connect-four__row">
                <Square value={state.squares[7]}/>
                <Square value={state.squares[8]}/>
                <Square value={state.squares[9]}/>
                <Square value={state.squares[10]}/>
                <Square value={state.squares[11]}/>
                <Square value={state.squares[12]}/>
                <Square value={state.squares[13]}/>
            </div>
            <div className="connect-four__row">
                <Square value={state.squares[14]}/>
                <Square value={state.squares[15]}/>
                <Square value={state.squares[16]}/>
                <Square value={state.squares[17]}/>
                <Square value={state.squares[18]}/>
                <Square value={state.squares[19]}/>
                <Square value={state.squares[20]}/>
            </div>
            <div className="connect-four__row">
                <Square value={state.squares[21]}/>
                <Square value={state.squares[22]}/>
                <Square value={state.squares[23]}/>
                <Square value={state.squares[24]}/>
                <Square value={state.squares[25]}/>
                <Square value={state.squares[26]}/>
                <Square value={state.squares[27]}/>
            </div>
            <div className="connect-four__row">
                <Square value={state.squares[28]}/>
                <Square value={state.squares[29]}/>
                <Square value={state.squares[30]}/>
                <Square value={state.squares[31]}/>
                <Square value={state.squares[32]}/>
                <Square value={state.squares[33]}/>
                <Square value={state.squares[34]}/>
            </div>
            <div className="connect-four__row">
                <Square value={state.squares[35]}/>
                <Square value={state.squares[36]}/>
                <Square value={state.squares[37]}/>
                <Square value={state.squares[38]}/>
                <Square value={state.squares[39]}/>
                <Square value={state.squares[40]}/>
                <Square value={state.squares[41]}/>
            </div>
            <div className="connect-four__row">
                <button className="connect-four__button" onClick={() => handleClick(0)}>1</button>
                <button className="connect-four__button" onClick={() => handleClick(1)}>2</button>
                <button className="connect-four__button" onClick={() => handleClick(2)}>3</button>
                <button className="connect-four__button" onClick={() => handleClick(3)}>4</button>
                <button className="connect-four__button" onClick={() => handleClick(4)}>5</button>
                <button className="connect-four__button" onClick={() => handleClick(5)}>6</button>
                <button className="connect-four__button" onClick={() => handleClick(6)}>7</button>
            </div>
            <div className="connect-four__row">
                <button className="connect-four__button" onClick={createNewGame}>Nouvelle partie</button>
            </div>
            <div className="connect-four__row">
                <div className="connect-four__text">Nombre de victoires du joueur vert: {state.greenVictories}</div>
                <div className="connect-four__text">Nombre de victoires du joueur rouge: {state.redVictories}</div>
            </div>
            {winner != null &&
                <div className="connect-four__victory">Le joueur {winner} gagne !</div>
            }
        </>
    );
};

export default ConnectFour;