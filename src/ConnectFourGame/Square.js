﻿import './Square.scss';

const Square = ({value}) => {
    
    const getClassName = () => {
        let className;
        switch (value){
            case 1:
                className = "square__button square__button--green";
                break;
            case 2:
                className = "square__button square__button--red";
                break;
            default:
                className = "square__button";
                break;
        }
        return className;
    }
    
    return(
        <button className={getClassName()} disabled></button>
    );
};

export default Square;