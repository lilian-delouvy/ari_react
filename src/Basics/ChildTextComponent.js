﻿import './ChildComponent.scss';

const ChildTextComponent = ({counter}) => {
    return(
        <div className="child-component__text">Vous avez cliqué {counter} fois !</div>
    );
};

export default ChildTextComponent;