﻿import {useState} from "react";
import './CustomButton.scss';
import './HelloWorld.scss';

const HelloWorld = () => {
    
    const [state, setState] = useState(
        {
            counter: 0
        }
    );
    
    return(
        <>
            <button className="button__custom" onClick={() => setState({counter: state.counter + 1})}>Cliquez ici !</button>
            <div className="hello-world__text">Hello World ! Vous avez cliqué {state.counter} fois !</div>
        </>
    );
    
};

export default HelloWorld;