﻿import HelloWorld from "./HelloWorld";
import CustomButton from "./CustomButton";
import Title from "../Title";
import ParentComponent from "./ParentComponent";

const onFirstButtonClick = () => {
    console.log("Bonjour !");
}

const onSecondButtonClick = () => {
    console.log("Bonsoir !");
}

const Basics = () => {
    return(
        <>
            <Title text="Bases"/>
            <a className="App-link" href="/">Retour à la page principale</a>
            <div className="button__container">
                <CustomButton onClick={onFirstButtonClick}/>
                <CustomButton onClick={onSecondButtonClick}/>
            </div>
            <HelloWorld/>
            <ParentComponent/>
        </>
    );
}

export default Basics;