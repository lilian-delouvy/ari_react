﻿const ChildButtonComponent = ({onClick}) => {
    return(
        <button className="button__custom" onClick={onClick}>Cliquez ici !</button>
    );
};

export default ChildButtonComponent;