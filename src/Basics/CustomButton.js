﻿import './CustomButton.scss';

const CustomButton = ({onClick}) => {
    
    return(
        <button className="button__custom" onClick={onClick}>Cliquez ici !</button>
    );
    
};

export default CustomButton;