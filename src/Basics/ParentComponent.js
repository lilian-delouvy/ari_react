﻿import ChildButtonComponent from "./ChildButtonComponent";
import ChildTextComponent from "./ChildTextComponent";
import {useState} from "react";

const ParentComponent = () => {
    
    const [state, setState] = useState(
        {
            counter: 0
        }
    );
    
    const updateCounter = () => {
        setState({...state, counter: state.counter + 1})
    }
    
    return(
        <>
            <ChildButtonComponent
                onClick={updateCounter}
            />
            <ChildTextComponent
                counter={state.counter}
            />
        </>
    );
};

export default ParentComponent;