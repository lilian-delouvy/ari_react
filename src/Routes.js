﻿import {Route, Switch} from "react-router-dom";
import Basics from "./Basics/Basics";
import ConnectFour from "./ConnectFourGame/ConnectFour";
import Home from "./Home";
import Clock from "./ThirdPage/Clock";

const Routes = () => {
    return(
        <Switch>
            <Route path="/basics">
                <Basics/>
            </Route>
            <Route path="/connectFour">
                <ConnectFour/>
            </Route>
            <Route path="/other">
                <Clock/>
            </Route>
            <Route path="/">
                <Home/>
            </Route>
        </Switch>
    );
};

export default Routes;